﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TareaDesarrolloWeb.Data;
using TareaDesarrolloWeb.Models;

namespace TareaDesarrolloWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationDbContext _context;

        public HomeController(ILogger<HomeController> logger, ApplicationDbContext context)
        {
             _context = context;
            _logger = logger;
           
        }

        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> CrearCliente([Bind("CodigoCliente, " +
            "NombreCliente","DireccionCliente","GeneroCliente","ProfesionCliente",
            "fechaNacimientoCliente","EdadCliente")] ClienteModel clienteModel)
        {
            if (ModelState.IsValid) {
                _context.Add(clienteModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(clienteModel);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
