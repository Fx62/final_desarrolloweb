﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TareaDesarrolloWeb.Models
{
    public class ClienteModel
    {
        [Key]
        [Required]
        public string CodigoCliente { get; set; }
        [Required]
        public string NombreCliente { get; set; }
        [Required]
        public string DireccionCliente { get; set; }
        [Required]
        public string GeneroCliente { get; set; }
        [Required]
        public string ProfesionCliente { get; set; }
        [Required]
        public string fechaNacimientoCliente { get; set; }
        [Required]
        public int EdadCliente { get; set; }

    }
}
