﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FinalDesarrollWeb.Models
{
    public class NotaCredito
    {
        [Display(Name = "Id")]
        public int id { get; set; }
        [Display(Name = "Id de venta")]
        public int sale { get; set; }
        [Display(Name = "Fecha")]
        public DateTime date { get; set; }
    }
}
