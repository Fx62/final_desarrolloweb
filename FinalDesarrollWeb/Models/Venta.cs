﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace FinalDesarrollWeb.Models
{
    public class Venta 
    {
        [Display(Name = "Id")]
        public int id { get; set; }
        [Display(Name = "Id de cliente")]
        public int client { get; set; }
        [Display(Name = "Fecha de venta")]
        public DateTime date { get; set; }
        [Display(Name = "Dirección")]
        public string address { get; set; }
        [Display(Name = "Estado")]
        public int status { get; set; }
        [Display(Name = "Total")]
        public double total { get; set; }
    }
}
