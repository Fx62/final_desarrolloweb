﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace FinalDesarrollWeb.Models
{
    public class DetalleVenta
    {
        [Display(Name = "Id")]
        public int id { get; set; }
        [Display(Name = "Id de Venta")]
        public int sale { get; set; }
        [Display(Name = "Cantidad")]
        public double amount { get; set; }
        [Display(Name = "Id de producto")]
        public int product { get; set; }
        [Display(Name = "Precio")]
        public double price { get; set; }
    }
}
