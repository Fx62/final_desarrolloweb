﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FinalDesarrollWeb.Models
{
    public class VendedorModel
    {
        [Display(Name = "Código de Usuario")]
        public int id { get; set; }

        [Display(Name = "ID de Usuario")]
        public string username { get; set; }

        [Display(Name = "Contraseña")]
        public string password { get; set; }

        [Display(Name = "Nombre")]
        public string firstName { get; set; }

        [Display(Name = "Apellido")]
        public string lastName { get; set; }

        [Display(Name = "NIT")]
        public int nit { get; set; }

        [Display(Name = "Correo Electrónico")]
        public string address { get; set; }

    }
}
