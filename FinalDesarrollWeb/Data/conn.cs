﻿using FinalDesarrollWeb.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinalDesarrollWeb.Data;

namespace FinalDesarrollWeb.Data
{
    public class conn : DbContext
    {
        public conn(DbContextOptions<conn> options) : base(options)
        {

        }
        public DbSet<VendedorModel> Vendedor { get; set; }
        public DbSet<Venta> Venta { get; set; }
        public DbSet<DetalleVenta> DetalleVenta { get; set; }
        public DbSet<NotaCredito> NotaCredito { get; set; }
    }
}
