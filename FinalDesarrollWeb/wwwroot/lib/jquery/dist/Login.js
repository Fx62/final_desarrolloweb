﻿jQuery(document).ready(function ($) {

    $('#txtUser').focus();
    $('#btnEntrar').on('click', function () {

        if ($('#txtUser').val() != "" & $('#txtPassword').val() != "") {
            Validate($('#txtUser').val(), $('#txtPassword').val());
        }
        else {
            Swal.fire(
                'Error',
                'Ingresar usuario y contraseña',
                'error'
            );
        }

    });
    function Validate(usuario, clave) {
        var record = {
            username: usuario,
            password: clave
        };
        $.ajax({
            url: '/VendedorModels/GetVendedor',
            async: false,
            type: 'POST',
            data: record,
            beforeSend: function (xhr, opts) {
            },
            complete: function () {
            },
            success: function (data) {
                if (data.status == true) {
                    window.location.href = "/Home/Index";
                }
                else if (data.status == false) {
                    Swal.fire(
                        'Error',
                        data.message,
                        'error'
                    );
                }
            },
            error: function (data) {
                Swal.fire(
                    'Error',
                    data.message,
                    'error'
                );
            }

        });



    }

});
