﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FinalDesarrollWeb.Data;
using FinalDesarrollWeb.Models;
using Microsoft.Extensions.Logging;

namespace FinalDesarrollWeb.Controllers
{
    public class VendedorModelsController : Controller
    {
        private readonly ILogger<VendedorModelsController> _logger;
        private readonly conn _context;

        public VendedorModelsController(conn context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult GetVendedor(string username, string password)
        {
            var usuario = _context.Vendedor.Where(s => s.username == username && s.password == password);

            if (usuario.Any())
            {
                if (usuario.Where(s => s.username == username && s.password == password).Any())
                {
                    return Json(new { status = true, message = "Bienvenido" });

                }
                else
                {
                    return Json(new { status = false, message = "Contraseña incorrecta" });


                }
            }
            else
            {
                return Json(new { status = false, message = "Usuario inválido" });
            }

        }

        // GET: VendedorModels
        public async Task<IActionResult> Index()
        {
            return View(await _context.Vendedor.ToListAsync());
        }

        // GET: VendedorModels/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vendedorModel = await _context.Vendedor
                .FirstOrDefaultAsync(m => m.id == id);
            if (vendedorModel == null)
            {
                return NotFound();
            }

            return View(vendedorModel);
        }

        // GET: VendedorModels/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: VendedorModels/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,username,password,firstName,lastName,nit,address")] VendedorModel vendedorModel)
        {
            if (ModelState.IsValid)
            {
                _context.Add(vendedorModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(vendedorModel);
        }

        // GET: VendedorModels/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vendedorModel = await _context.Vendedor.FindAsync(id);
            if (vendedorModel == null)
            {
                return NotFound();
            }
            return View(vendedorModel);
        }

        // POST: VendedorModels/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("id,username,password,firstName,lastName,nit,address")] VendedorModel vendedorModel)
        {
            if (id != vendedorModel.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(vendedorModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!VendedorModelExists(vendedorModel.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(vendedorModel);
        }

        // GET: VendedorModels/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vendedorModel = await _context.Vendedor
                .FirstOrDefaultAsync(m => m.id == id);
            if (vendedorModel == null)
            {
                return NotFound();
            }

            return View(vendedorModel);
        }

        // POST: VendedorModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var vendedorModel = await _context.Vendedor.FindAsync(id);
            _context.Vendedor.Remove(vendedorModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool VendedorModelExists(int id)
        {
            return _context.Vendedor.Any(e => e.id == id);
        }
    }
}
