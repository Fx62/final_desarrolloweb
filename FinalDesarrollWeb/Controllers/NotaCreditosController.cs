﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinalDesarrollWeb.Data;
using FinalDesarrollWeb.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace FinalDesarrollWeb.Controllers
{
    public class NotaCreditosController : Controller
    {
        private readonly conn _context;

        public NotaCreditosController(conn context)
        {
            _context = context;
        }

        // GET: NotaCreditos
        public async Task<IActionResult> Index()
        {
            return View(await _context.NotaCredito.ToListAsync());
        }

        // GET: NotaCreditos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notaCredito = await _context.NotaCredito
                .FirstOrDefaultAsync(m => m.id == id);
            if (notaCredito == null)
            {
                return NotFound();
            }

            return View(notaCredito);
        }

        // GET: NotaCreditos/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: NotaCreditos/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,sale,date")] NotaCredito notaCredito)
        {
            if (ModelState.IsValid)
            {
                _context.Add(notaCredito);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(notaCredito);
        }

        // GET: NotaCreditos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notaCredito = await _context.NotaCredito.FindAsync(id);
            if (notaCredito == null)
            {
                return NotFound();
            }
            return View(notaCredito);
        }

        // POST: NotaCreditos/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("id,sale,date")] NotaCredito notaCredito)
        {
            if (id != notaCredito.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(notaCredito);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NotaCreditoExists(notaCredito.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(notaCredito);
        }

        // GET: NotaCreditos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notaCredito = await _context.NotaCredito
                .FirstOrDefaultAsync(m => m.id == id);
            if (notaCredito == null)
            {
                return NotFound();
            }

            return View(notaCredito);
        }

        // POST: NotaCreditos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var notaCredito = await _context.NotaCredito.FindAsync(id);
            _context.NotaCredito.Remove(notaCredito);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool NotaCreditoExists(int id)
        {
            return _context.NotaCredito.Any(e => e.id == id);
        }
    }
}
